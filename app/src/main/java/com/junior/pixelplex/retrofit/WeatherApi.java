package com.junior.pixelplex.retrofit;

import com.junior.pixelplex.retrofit.apiPojo.OpenWeatherMapResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface WeatherApi {

    @GET("/data/2.5/forecast")
    Call<OpenWeatherMapResponse> getWeatherForecast(@Query("lat") double lat,
                                             @Query("lon") double lon,
                                             @Query("appid") String appid,
                                             @Query("units") String units,
                                             @Query("lang") String lang);

}
