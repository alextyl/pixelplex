package com.junior.pixelplex;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.junior.pixelplex.broadcastReceivers.WeatherBroadcastReceiver;
import com.junior.pixelplex.model.WeatherItem;
import com.junior.pixelplex.services.WeatherRequestService;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class WeatherActivity extends AppCompatActivity implements LocationListener, WeatherCallback{

    private static final int LOCATION_REQUEST_CODE = 1;
    private LocationManager locationManager;
    private SharedPreferences sharedPrefCoor;
    private SharedPreferences sharedPrefWeath;
    private BroadcastReceiver weatherBrodcastReceiver;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        sharedPrefCoor = this.getSharedPreferences("coordinates", Context.MODE_PRIVATE);
        sharedPrefWeath = this.getSharedPreferences("weather", Context.MODE_PRIVATE);
        weatherBrodcastReceiver = new WeatherBroadcastReceiver(this);
        IntentFilter intentFilter = new IntentFilter(WeatherRequestService.ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(weatherBrodcastReceiver, intentFilter);
        listView = findViewById(R.id.list);


        locationCheckingOn();

        setWeather();

    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(weatherBrodcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(WeatherRequestService.ACTION);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(weatherBrodcastReceiver, intentFilter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case LOCATION_REQUEST_CODE: {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    locationChecking();
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            LOCATION_REQUEST_CODE);
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences.Editor editor = sharedPrefCoor.edit();
        editor.putFloat("lat", (float) location.getLatitude());
        editor.putFloat("lon", (float) location.getLongitude());
        editor.apply();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void locationCheckingOn(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST_CODE);
        } else {
            locationChecking();
        }
    }

    @SuppressLint("MissingPermission")
    private void locationChecking(){
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        } catch (NullPointerException e){
            e.printStackTrace();
        }

        Intent intent = new Intent(this, WeatherRequestService.class);
        startService(intent);
    }


    @Override
    public void updateWeather() {
        setWeather();
    }

    private void setWeather(){
        ArrayList<WeatherItem> weatherItems = new ArrayList<>();
        for(int i = 0; i <= 39; i++){
            WeatherItem weatherItem = new WeatherItem(sharedPrefWeath.getFloat("temp_" + i, 0),
                    sharedPrefWeath.getLong("humidity_" + i, 0),
                    sharedPrefWeath.getString("description_" + i, ""),
                    sharedPrefWeath.getFloat("speed_" + i, 0),
                    sharedPrefWeath.getString("dttxt_" + i, ""),
                    sharedPrefWeath.getString("icon_" + i, ""));
            weatherItems.add(weatherItem);
        }

        listView.setAdapter(new WeatherAdapter(this, weatherItems));
    }

    private class WeatherAdapter extends ArrayAdapter<WeatherItem>{

        public WeatherAdapter(@NonNull Context context,  @NonNull List<WeatherItem> objects) {
            super(context, 0, objects);
        }


        @SuppressLint("SetTextI18n")
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            final WeatherItem weatherItem = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_elem, parent, false);
            }

            DecimalFormat df = new DecimalFormat("#.##");

            TextView textView1 = convertView.findViewById(R.id.textView1);
            TextView textView2 = convertView.findViewById(R.id.textView2);
            TextView textView3 = convertView.findViewById(R.id.textView3);
            TextView textView4 = convertView.findViewById(R.id.textView4);
            ImageView imageView = convertView.findViewById(R.id.imageView1);

            textView1.setText(weatherItem.getDescription() + "\n" + weatherItem.getDttxt());
            textView2.setText(df.format(weatherItem.getTemp())  + " t°");
            textView3.setText(df.format(weatherItem.getHumidity()) + "% влажности");
            textView4.setText(df.format(weatherItem.getSpeed()) + " м/с");

            Picasso.with(WeatherActivity.this)
                    .load(weatherItem.getIcon())
                    .placeholder(R.mipmap.ic_launcher)
                    .into(imageView);

            return convertView;
        }
    }


}
