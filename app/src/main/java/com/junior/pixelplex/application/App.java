package com.junior.pixelplex.application;

import android.app.Application;
import android.util.Log;

import com.junior.pixelplex.retrofit.WeatherApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class App extends Application {

    private static WeatherApi WeatherApi;
    private static final String APIID = "701daf357de3d4b8a2151308b096ffbd";
    private Retrofit retrofit;

    public static com.junior.pixelplex.retrofit.WeatherApi getWeatherApi() {
        return WeatherApi;
    }

    public static String getAPIID() {
        return APIID;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        WeatherApi = retrofit.create(WeatherApi.class);

    }
}
