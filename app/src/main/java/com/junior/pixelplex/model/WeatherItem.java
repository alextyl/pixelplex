package com.junior.pixelplex.model;

/**
 * Created by Алексей on 10.04.2018.
 */

public class WeatherItem {


    private double temp;
    private long humidity;
    private String description;
    private double speed;
    private String dttxt;
    private String icon;


    public WeatherItem(double temp, long humidity, String description, double speed, String dttxt, String icon) {
        this.temp = temp;
        this.humidity = humidity;
        this.description = description;
        this.speed = speed;
        this.dttxt = dttxt;
        this.icon = "http://openweathermap.org/img/w/" + icon + ".png" ;
    }


    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public long getHumidity() {
        return humidity;
    }

    public void setHumidity(long humidity) {
        this.humidity = humidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getDttxt() {
        return dttxt;
    }

    public void setDttxt(String dttxt) {
        this.dttxt = dttxt;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
