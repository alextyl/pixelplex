package com.junior.pixelplex.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.junior.pixelplex.application.App;
import com.junior.pixelplex.retrofit.apiPojo.OpenWeatherMapResponse;


import retrofit2.Response;


public class WeatherRequestService extends IntentService {

    public static final String ACTION = "com.junior.pixelplex.WeatherServiceResponse";

    public WeatherRequestService() {
        super("WeatherRequestService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        SharedPreferences sharedPrefCoor = getApplicationContext().getSharedPreferences("coordinates",
                Context.MODE_PRIVATE);

        SharedPreferences sharedPrefWeath = getApplicationContext().getSharedPreferences("weather",
                Context.MODE_PRIVATE);

        while (true){
            double lat = sharedPrefCoor.getFloat("lat", -1);
            double lon = sharedPrefCoor.getFloat("lon", -1);

            if(lat != -1 && lon != -1){
                try {
                Response<OpenWeatherMapResponse> openWeatherMapResponse = App.getWeatherApi().getWeatherForecast(lat, lon, App.getAPIID(), "metric", "ru").execute();
                if(openWeatherMapResponse.code() == 200){
                    for(int i = 0; i <= 39; i++){
                        SharedPreferences.Editor editor = sharedPrefWeath.edit();
                        editor.putFloat( "temp_" + i, (float) openWeatherMapResponse.body().getList().get(i).getMain().getTemp());
                        editor.putLong("humidity_" + i, openWeatherMapResponse.body().getList().get(i).getMain().getHumidity());
                        editor.putString("description_" + i, openWeatherMapResponse.body().getList().get(i).getWeather().get(0).getDescription());
                        editor.putFloat("speed_" + i, (float) openWeatherMapResponse.body().getList().get(i).getWind().getSpeed());
                        editor.putString("dttxt_" + i, openWeatherMapResponse.body().getList().get(i).getDtTxt());
                        editor.putString("icon_" + i, openWeatherMapResponse.body().getList().get(i).getWeather().get(0).getIcon());
                        editor.commit();
                    }
                }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent responseIntent = new Intent();
                responseIntent.setAction(ACTION);
                responseIntent.addCategory(Intent.CATEGORY_DEFAULT);
                sendBroadcast(responseIntent);
            }

            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
