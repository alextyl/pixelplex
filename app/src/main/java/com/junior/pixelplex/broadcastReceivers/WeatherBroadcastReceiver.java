package com.junior.pixelplex.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.junior.pixelplex.WeatherCallback;


public class WeatherBroadcastReceiver extends BroadcastReceiver {

    private WeatherCallback weatherCallback;

    public WeatherBroadcastReceiver(WeatherCallback weatherCallback) {
        this.weatherCallback = weatherCallback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        weatherCallback.updateWeather();
    }
}
